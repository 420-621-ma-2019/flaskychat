#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/err.h>
#include <linux/sysfs.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/delay.h>
#include <linux/gpio.h>


//les valeurs "magiques" pour les CMD et DATA requests de l'écran
#define CMD_REQUEST	0x00
#define DATA_REQUEST	0x40
//le # de GPIO pour le reset de l'écran
#define GPIO_RST_ECRAN	49
#define GPIO_RST_ECRAN_LABEL "ECRAN_RST_PIN"
//un nom qui sera le même que dans l'overlay
#define PILOTE_NOM	"Jacques_Villeneuve"

MODULE_AUTHOR("Laurent et Felix ... et david :))");
MODULE_DESCRIPTION("Creation de fichier pour ecrire a lecran");
MODULE_LICENSE("GPL");

/*
 *******************************************************************************
 * Déclarations (pour éviter un fichier .h)
 */
uint8_t i;
uint8_t nb;
 
// Fonctions de pilote-module
static int chip_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id);
static int chip_i2c_remove(struct i2c_client *client);
int chip_write_value(u8 reg, u16 value);

// Fonctions de sysfs
static int ecran_sysfs_init(void);
static ssize_t ecran_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count);
static ssize_t ecran_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
int initRST(void);
void exitRST(void);
void ctrlRST(char etat);

// Fonctions contrôle de l'écran
int init_ecran(void);
void command(char data);
void data(char data);

/*
 *******************************************************************************
 * Structures pour le kernel
 */
static const unsigned short normal_i2c[] = {0x3C, I2C_CLIENT_END};
static struct kobject *ecran_kobj = NULL;
static const struct i2c_device_id chip_i2c_id[] = {
    {PILOTE_NOM, 0},
    {}};
static struct kobj_attribute ecran_attr = __ATTR_RW(ecran);
static struct i2c_driver chip_driver = {
    .class = I2C_CLASS_HWMON,
    .driver = {
        .name = PILOTE_NOM,
    },
    .probe = chip_i2c_probe,
    .remove = chip_i2c_remove,
    .id_table = chip_i2c_id,
    .address_list = normal_i2c,
};
static struct attribute *ebb_attrs[] = {
    &ecran_attr.attr,
    NULL,
};
static struct attribute_group attr_group = {
    .name = "ecran",
    .attrs = ebb_attrs,
};
MODULE_DEVICE_TABLE(i2c, chip_i2c_id);

/*
 * La macro ci-dessous fait la même chose que les macros module_init et module_exit.
 * Les fonctions chip_driver_probe et chip_i2c_remove sont enregistrées comme les points
 * d'entrée et de sortie du module.
 */
module_i2c_driver(chip_driver);

/*
 *******************************************************************************
 * Autres structures et variables globales
 */
static struct i2c_client *chip_i2c_client = NULL;
char string_ecran[81];

/*
 *******************************************************************************
 * Fonctions
 */

/*
 * Initialisation du device sur le bus i2c
 */
static int chip_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    chip_i2c_client = client;
    init_ecran();
    ecran_sysfs_init();
    return 0;
}

/*
 * Déinitialisation du device sur le bus i2c
 */
static int chip_i2c_remove(struct i2c_client *client)
{
    exitRST();
    chip_i2c_client = NULL;
    kobject_put(ecran_kobj);
    return 0;
}

/*
 * Modifie un registre de l'écran par i2c
 */
int chip_write_value(u8 reg, u16 value)
{
    int ret = 0;

    /*
     * La fonction ci-dessous est fournie par le pilote i2c générique et nous permet
     * d'écrire sur le bus i2c directement. Elle est déjà configurée pour parler à
     * la bonne adresse.
     */
    ret = i2c_smbus_write_byte_data(chip_i2c_client, reg, value);
    return ret;
}

/*
 * Crée les fichiers nécessaires au fonctionnement du driver dans sysfs
 */
static int ecran_sysfs_init(void)
{
    int result = 0;

    ecran_kobj = kobject_create_and_add("jalon3ecran", kernel_kobj->parent);
    if (!ecran_kobj) {
        printk(KERN_ALERT "device-ecran: impossible de creer le repertoire sysfs\n");
        return -1;
    }

    result = sysfs_create_group(ecran_kobj, &attr_group);
    if (result) {
        printk(KERN_ALERT "device-ecran: impossible de creer le sous-groupe sysfs de l'ecran\n");
        kobject_put(ecran_kobj); // clean up
        return result;
    }
    return result;
}

/*
 * Fonction associée à l'objet sysfs "ecran".
 * Lit et stocke le texte entré par l'utilisateur puis le fait afficher sur l'écran
 */
static ssize_t ecran_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
    /*
     * Dans cette fonction, vous devez prendre les caractères reçus du système qui sont dans "buf" et les
     * faire afficher à l'écran. "count" est la quantité de caractères que contient "buf".
     */

	command(0x80);
    if ( count > 80 )
    {
		nb = 80;
    }
    else if (count <= 80)
    {
        nb = count;
    }

	for(i = 0;i < nb; i++)
	{   
        string_ecran[i] = buf[i];
		data(string_ecran[i]);
    }
	
	return count;
}

/*
 * Fonction associée à l'objet sysfs "ecran".
 * Retourne le contenu actuel de l'écran.
 */
static ssize_t ecran_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
    /*
     * Dans cette fonction, vous devez inscrire dans "buf" les caractères qui sont présentement à l'écran
     */

    char string_buf[84];
    for( i = 0; i < 84; i++)
    {
        if(i == 20)
        {
            string_buf[i] = 0x13;

        }
        else if (i == 40)
        {
            string_buf[i] = 0x13;
        }
        else if (i == 60)
        {
            string_buf[i] = 0x13;
        }
        else
        {
            string_buf[i] = string_ecran[i];
        }
        
    }

	//return sprintf(buf, "%s", string_ecran);
    return sprintf(buf, "%s", string_buf);
  
}

/*
 * Routine d'initialisation i2c de l'écran
 */
int init_ecran()
{
    int result = 0;

    /*
     * Cette fonction doit faire la routine d'initialisation i2c de l'écran.
     * Vous devez transcrire la fonction du jalon 2 avec les fonctions qui sont disponibles ici.
     */
     
    result = initRST();
    ctrlRST('0');
    msleep(1);
    ctrlRST('1');
        
    msleep(3);
    
    // Séquence de lancement de l'écran
    command(0x2A);      // function set (extended command set)
    command(0x71);      // function selection A
    data(0x00);         // disable internal VDD regulator (2.8V I/O). data(0x5C) = enable regulator (5V I/O)
    command(0x28);      // function set (fundamental command set)
    command(0x08);      // display off, cursor off, blink off
    command(0x2A);      // function set (extended command set)
    command(0x79);      // OLED command set enabled
    command(0xD5);      // set display clock divide ratio/oscillator frequency
    command(0x70);      // set display clock divide ratio/oscillator frequency
    command(0x78);      // OLED command set disabled
    command(0x09);      // extended function set (4-lines)
    command(0x06);      // COM SEG direction
    command(0x72);      // function selection B
    data(0x00);         // ROM CGRAM selection
    command(0x2A);      // function set (extended command set)
    command(0x79);      // OLED command set enabled
    command(0xDA);      // set SEG pins hardware configuration
    command(0x10);      // set SEG pins hardware configuration
    command(0xDC);      // function selection C
    command(0x00);      // function selection C
    command(0x81);      // set contrast control
    command(0x7F);      // set contrast control
    command(0xD9);      // set phase length
    command(0xF1);      // set phase length
    command(0xDB);      // set VCOMH deselect level
    command(0x40);      // set VCOMH deselect level
    command(0x78);      // OLED command set disabled
    command(0x28);      // function set (fundamental command set)
    command(0x01);      // clear display
    command(0x80);      // set DDRAM address to 0x00
    command(0x0C);      // display ON
    
    msleep(3);
    printk(KERN_ALERT"fin de init ecran");
    return result;
}

/*
 * Envoie une "commande" à l'écran
 */
void command(char data)
{
    chip_write_value(CMD_REQUEST,data);
}

/*
 * Envoie une "donnée" à l'écran
 */
void data(char data)
{
    chip_write_value(DATA_REQUEST,data);
}

/*
 * Rend le GPIO associé à la pin "reset" de l'écran disponible au pilote et limite les accès pour le système
 * en général.
 */
int initRST(void)
{
    int result = 0;

    result = gpio_request(GPIO_RST_ECRAN, GPIO_RST_ECRAN_LABEL);
    if (result) {
        printk(KERN_ALERT "device-ecran: le GPIO pour le reset de l'ecran n'est pas disponible.\n");
        return result;
    }
    gpio_direction_output(GPIO_RST_ECRAN, true);
    gpio_export(GPIO_RST_ECRAN, false);

    return 0;
}

/*
 * Libère le GPIO associé à la pin "reset" de l'écran pour le rendre disponible à nouveau au reste du système.
 */
void exitRST(void)
{
    gpio_set_value(GPIO_RST_ECRAN, 0);
    gpio_unexport(GPIO_RST_ECRAN);
    gpio_free(GPIO_RST_ECRAN);
}

/*
 * Contrôle l'état de la pin "reset" de l'écran
 */
void ctrlRST(char etat)
{
    gpio_set_value(GPIO_RST_ECRAN, (int) etat);
}
