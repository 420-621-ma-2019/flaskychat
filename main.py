#	 _____ _         _       _____ _       _
#	|   __| |___ ___| |_ _ _|     | |_ ___| |_
#	|   __| | .'|_ -| '_| | |   --|   | .'|  _|
#	|__|  |_|__,|___|_,_|_  |_____|_|_|__,|_|
#	                    |___|
#
#	Laurent Mailloux-Bourassa
#	David Caron

#Imports
from flask import Flask, render_template, request
from flask_socketio import SocketIO
import netifaces as ni
import random
import lxml.html
import time
import datetime

#Network settings
ifconfig_ubuntu = 'wlp8s0'
ifconfig_kubuntu =  'wlp2s0'
ifconfig_lubuntu = 'enp0s31f6'
ifconfig_beaglebone = 'eth0'
ifconfig = ""
port = '1337'

#Chat code
code = ""

#Online users
used_names = []
online_users = 0
new_connection = False
new_user = ""

#Pages
pages = []

#Bot class
class Bot:
	name = "Flaskybot"
	message = ""
used_names.append(Bot.name)

#Bot commands
bot_commands = ["/hello","/help","/online"]

#Network interface configuration
interfaces = ni.interfaces()
if ifconfig_lubuntu in interfaces:
	ifconfig = ifconfig_lubuntu
elif ifconfig_beaglebone in interfaces:
	ifconfig = ifconfig_beaglebone
elif ifconfig_ubuntu in interfaces:
	ifconfig = ifconfig_ubuntu
elif ifconfig_kubuntu in interfaces:
	ifconfig = ifconfig_kubuntu

#Random code generator
for i in range(5):
	code = code + str(random.randrange(10))

#Server creation
flaskychat = Flask(__name__)
socketio = SocketIO(flaskychat)
ni.ifaddresses(ifconfig)
ip = ni.ifaddresses(ifconfig)[ni.AF_INET][0]['addr']

#Ajoute les informations de l'instance de chat à log.txt
#et affiche ceux-ci sur l'écran LCD si le programme roule
#sur le Beaglebone.
if(ifconfig == ifconfig_beaglebone):
	t = time.time()
	dt = datetime.datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')

	f = open("/home/debian/flaskychat/log.txt", "a")
	f.write("\n\r" + str(dt))
	f.write("\n\rAdresse IP:" + ip + "  Code:"+ code)
	f.write("\n\r__________________________________")
	f.close()

	f = open("/sys/jalon3ecran/ecran/ecran", "w")
	f.write("                                  ")
	f.close()

	f = open("/sys/jalon3ecran/ecran/ecran", "w")
	f.write(ip + ":" + port + "    " + code)
	f.close()

print("Connectez-vous au serveur sur " + ip + ":" + port)
print("Entrez le code " + code)

#Landing page
#Cette fonction affiche
#la page d'accueil du chat.
@flaskychat.route('/')
def landing():
    return render_template('landing.html')

#Authorization process
#Fonction qui reçoit les informations envoyées
#par l'utilisateur depuis la page landing.html
#où il saisie son pseudo, le chatroom et le code
#du serveur. La fonction traite les erreurs
#s'il y a lieu, sinon, il redirige l'utilisateur
#vers le chat.
@flaskychat.route('/postresponse', methods=['POST'])
def postresponse():
	#Form reception
	pseudo = request.form['user']
	got_id = request.form['chatid']
	got_code = request.form['code']

	#Error manager
	if got_code != code:
		return (render_template('landing.html') + "Wrong password!")
	elif pseudo in used_names:
		return (render_template('landing.html') + "Username already in use!")
	elif pseudo == "":
		return (render_template('landing.html') + "Please enter a username!")
	elif got_id == "":
		return (render_template('landing.htm') + "Please specify a chat ID!")

	#Create user and open chatroom
	else:
		global new_connection
		global new_user
		global online_users
		global new_connection_server

		if id not in pages:
			pages.append(id)

		new_connection = True
		new_user = pseudo
		online_users += 1
		new_connection_server = got_id
		used_names.append(pseudo)
		return render_template('chat.html', Pseudo = pseudo, ChatID = got_id)

#Chat
#Cette fonction affiche
#la page de chat.
@flaskychat.route('/chat')
def chat():
	return render_template('chat.html')

#Chat event
#Fonction qui fonctionne comme une interruption.
#S'active lorsqu'un utilisateur appuie sur Enter
#ou lorsqu'un nouvel utilisateur entre dans le
#chat. La fonction filtre les messages vides et
#les tentatives d'injections HTML. Elle détecte
#également les commandes commençant par "/"
#destinées au Bot du chat et celui-ci répond
#en fonction de la commande.
@socketio.on('my event')
def chatevent(json, methods = ['GET', 'POST']):
	#New connection
	global new_connection
	if new_connection == True:
		global new_user
		global new_connection_server
		new_connection = False
		Bot.message = "Welcome to Flaskychat " + new_user + "! I am " + Bot.name + ". Type /help to see a list of commands you can use."
		socketio.emit('my response', {'user_name':Bot.name,'message':Bot.message,'chat_id':new_connection_server})

	#Message reception
	else:
		#Message cleanup (Anti-Ben)
		responseJson = ('my response', json)
		responseList = list(responseJson)
		responseValues = responseList[1]
		message = responseValues['message']
		chatID = responseValues['chat_id']
		cleanMessage = lxml.html.fromstring(message)
		cleanMessage.text_content()
		json['message'] = cleanMessage.text_content()
		json['chat_id'] = chatID

		#Bot and response
		if(message[0] == "/"):
			if message == "/hello":
				Bot.message = "Hello " + json['user_name'] +  "! My name is " + Bot.name + "."
			elif message == "/help":
				Bot.message = "Here is a list of commands you can use:"
			elif message == "/online":
				Bot.message = "There are currently " + str(online_users) + " online users:"
			else:
				Bot.message = "I do not understand this command... Type /help to see a list of commands you can use."

			#Bot command response
			socketio.emit('my response', json)
			socketio.emit('my response', {'user_name':Bot.name,'message':Bot.message,'chat_id':chatID})

			if message == "/help":
				for command in bot_commands:
					socketio.emit('my response',{'user_name':"",'message':command,'chat_id':chatID})
			if message == "/online":
				for user in used_names:
					if user is not Bot.name:
						socketio.emit('my response',{'user_name':"",'message':user,'chat_id':chatID})
		else:
			socketio.emit('my response', json)

#Main (Wait for events)
if __name__ == '__main__':
	socketio.run(flaskychat,host = ip,port = port)
