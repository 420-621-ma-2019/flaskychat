import hellow
from flask import request, render_template

@hellow.route('/')
@hellow.route('/index')
def index():
	return "Hello world!"

@hellow.route('/personalhello')
def hello():
	nom = request.args.get('n')
	prenom = request.args.get('p')
	return "<h1>Hello, " + prenom + " " + nom + "</h1>"

@hellow.route('/templatehello')
def tplhello():
	nom = request.args.get('n')
	prenom = request.args.get('p')
	return render_template('hellotpl.html', leprenom=prenom, lenom=nom)

@hellow.route('/gethello')
def gethello():
	return render_template('formget.html')

@hellow.route('/posthello')
def posthello():
	return render_template('formpost.html')

@hellow.route('/postresponse', methods=['POST'])
def postresp():
	nom = request.form['n']
	prenom = request.form['p']
	return render_template('hellotpl.html', leprenom=prenom, lenom=nom)
